var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var PatientBMI = 0;
/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
	sessionId = getSessionId();
	
	switch(stPacienta) {
		case 1: document.getElementById("kreirajIme").value = "Alan";
				document.getElementById("kreirajPriimek").value = "Turing";
				kreirajEHR();
				document.getElementById("kreirajVisino").value = "170";
				document.getElementById("kreirajTezino").value = "67";
				dodajMeritev();
				break;
		case 2: document.getElementById("kreirajIme").value = "Adam";
				document.getElementById("kreirajPriimek").value = "Savage";
				kreirajEHR();
				document.getElementById("kreirajVisino").value = "181";
				document.getElementById("kreirajTezino").value = "75";
				dodajMeritev();
				break;
		case 3: document.getElementById("kreirajIme").value = "Johnny";
				document.getElementById("kreirajPriimek").value = "Alpha";
				kreirajEHR();
				document.getElementById("kreirajVisino").value = "189";
				document.getElementById("kreirajTezino").value = "96";
				dodajMeritev();
				break;
	}
	document.getElementById("kreirajIme").value = "";
	document.getElementById("kreirajPriimek").value = "";
	document.getElementById("EhrId").value = "";
	document.getElementById("kreirajVisino").value = "";
	document.getElementById("kreirajTezino").value = "";
	
}

function kreirajEHR() {
    sessionId = getSessionId();

    var ime = $("#kreirajIme").val();
    var priimek = $("#kreirajPriimek").val();

    if (!ime || !priimek || ime.trim().length == 0 || priimek.trim().length == 0 ) {
        $("#kreirajSporocilo").html("<div class='alert alert-dismissible alert-warning'><button type='button' class='close' data-dismiss='alert'>&times;</button>Prosim vnesi EHRid!</div>");
    }
    else {
        $.ajaxSetup({
            async: false,
            headers: {
                "Ehr-Session": sessionId
            }
        });
        $.ajax({
            async: false,
            url: baseUrl + "/ehr",
            type: "POST",
            success: function(data) {
                var ehrId = data.ehrId;
				document.getElementById("EhrId").value = ehrId;
                var partyData = {
                    firstNames: ime,
                    lastNames: priimek,
                    partyAdditionalInfo: [{
                        key: "ehrId",
                        value: ehrId
                    }]
                };
                $.ajax({
                    async: false,
                    url: baseUrl + "/demographics/party",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(partyData),
                    success: function(party) {
                        if (party.action == "CREATE") {
                            $("#kreirajSporocilo").html("<div class='alert alert-dismissible alert-success'><button type='button' class='close' data-dismiss='alert'>&times;</button>Uspešno kreiran EHR: " + ehrId + "</div>");
							var x = document.createElement("TR");
							x.setAttribute("id", ehrId);
							document.getElementById("podatki").appendChild(x);
							var y = document.createElement("TD");
							y.setAttribute("id", "bolnik");
							var t = document.createTextNode(ime + " " + priimek);
							y.appendChild(t);
							var s= document.createElement("TD");
							s.setAttribute("id", "id_pacient");
							var p= document.createTextNode(ehrId);
							s.appendChild(p);
							document.getElementById(ehrId).appendChild(y);
							document.getElementById(ehrId).appendChild(s);
                        }
                    }
                });
            }
        });
    }
}

function dodajMeritev() {
	sessionId = getSessionId();
	var ehrId = $("#EhrId").val();
	var visina = $("#kreirajVisino").val();
	var teza = $("#kreirajTezino").val();
	if (!ehrId || !visina || !teza || ehrId.trim().length == 0 || visina.trim().length == 0 || teza.trim().length == 0 ) {
		$("#kreirajSporocilo2").html("<div class='alert alert-dismissible alert-warning'><button type='button' class='close' data-dismiss='alert'>&times;</button>Prosim vnesite zahtevane podatke!</div>");
	} else {
		$.ajaxSetup({
            async: false,
            headers: {
                "Ehr-Session": sessionId
            }
        });
		
		var podatki = {
			"ctx/language": "en",
		    "ctx/territory": "SI",
			"vital_signs/height_length/any_event/body_height_length": visina,
		    "vital_signs/body_weight/any_event/body_weight": teza
		};
		
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT'
		};
		
		$.ajax({
			url: baseUrl + "/composition?" + $.param(parametriZahteve),
		    type: 'POST',
		    contentType: 'application/json',
		    data: JSON.stringify(podatki),
			success: function(res) {
				$("#kreirajSporocilo2").html("<div class='alert alert-dismissible alert-success'><button type='button' class='close' data-dismiss='alert'>&times;</button>Uspešno dodane meritve. "+ "</div>");
			}
		});
	}
}
function generateMessage(bmiVal){
	if(bmiVal > 0 && bmiVal <= 22.1) return "Germany";
	if(bmiVal > 22.3 && bmiVal <= 22.4) return "France";
	if(bmiVal > 22.45 && bmiVal <= 23.10) return "Italy";
	if(bmiVal > 23.11 && bmiVal <= 23.90) return "Austria";
	if(bmiVal > 23.91 && bmiVal <= 24.31) return "Slovenia";
	if(bmiVal > 24.32 && bmiVal <= 24.61) return "Denmark";
	if(bmiVal > 24.62 && bmiVal <= 24.91) return "Ireland";
	if(bmiVal > 24.92 && bmiVal <= 25.17) return "Spain";
	if(bmiVal > 25.17 && bmiVal <= 25.48) return "Belgium";
	if(bmiVal > 25.48 && bmiVal <= 25.90) return "Greece";
	if(bmiVal > 25.90 && bmiVal <= 26.10) return "Netherlands";
	if(bmiVal > 26.10 && bmiVal <= 26.40) return "UK";
	else
		return "Malta.";
}
var nr = 1;
function preberiEHR() {
	sessionId = getSessionId();
	var visina,teza,result, printRes;
	var ehrId = $("#EhrId2").val();
	if(!ehrId || ehrId.trim().length == 0 ) {
		$("#kreirajSporocilo3").html("<div class='alert alert-dismissible alert-warning'><button type='button' class='close' data-dismiss='alert'>&times;</button>Prosim vnesite zahtevane podatke!</div>");
	} else {
		$.ajaxSetup({
            async: false,
            headers: {
                "Ehr-Session": sessionId
            }
        });
		$.ajax ({
			url: baseUrl + "/view/" + ehrId + "/height",
			type: 'GET',
			headers: {
				"Ehr-Session": sessionId
			},
			success: function(data) {
				visina = data[0].height;
				result = "Visina: " + visina + "<br/><br/>";
			}
		});
		$.ajax ({
			url: baseUrl + "/view/" + ehrId + "/weight",
			type: 'GET',
			headers: {
				"Ehr-Session": sessionId
			},
			success: function(data) {
				teza = data[0].weight;
				PatientBMI = Math.round(teza / ((visina*visina)/10000));
				 printRes = generateMessage(PatientBMI);
				result += "Tezina: " + teza + "<br/><br/> \
						   BMI: " + PatientBMI + "<br/><br/>" + "Results: " +  printRes + "<br/></br>";
				printRes = "I%20am%20from%20" + printRes +"%20according%20to%20my%20BMI!";
				$("#chart").html(graphBar(PatientBMI));
				$("#result").html(result);
				$('#tvitni').append("<br></br><a class='twitter-share-button' href='https://twitter.com/intent/tweet?text="+printRes+"'data-size='large'><img src='https://static.addtoany.com/images/blog/tweet-button-2015.png' style='width:100px'></a></p>");
			}
		});
	}
}
function graphBar(bmiVal){
 let myChart = document.getElementById('chart').getContext('2d');

    // Global Options
    Chart.defaults.global.defaultFontFamily = 'Lato';
    Chart.defaults.global.defaultFontSize = 18;
    Chart.defaults.global.defaultFontColor = '#777';
    let massPopChart = new Chart(myChart, {
      type:'horizontalBar', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
      data:{
        labels:['You', 'Germany','France','Italy','Austria','Sweden','Denmark','Ireland','Spain','Belgium','Greece','Netherlands','UK'],
        datasets:[{
          label:'You',
          data:[
            bmiVal,22.1,22.4,23.10,23.0,24.31,24.61,24.91,25.17,25.48,25.90,26.10,26.40],
          //backgroundColor:'green',
          backgroundColor:[
            'rgba(255, 0, 0, 0.6)',
            'rgba(255, 255, 0, 0.6)',
        	'rgba(255, 255, 0, 0.6)',
            'rgba(255, 255, 0, 0.6)',
            'rgba(255, 255, 0, 0.6)',
            'rgba(255, 255, 0, 0.6)',
            'rgba(255, 255, 0, 0.6)',
            'rgba(255, 255, 0, 0.6)',
        	'rgba(255, 255, 0, 0.6)',
            'rgba(255, 255, 0, 0.6)',
            'rgba(255, 255, 0, 0.6)',
            'rgba(255, 255, 0, 0.6)',
            'rgba(255, 255, 0, 0.6)'
          ],
          borderWidth:1,
          borderColor:'#777',
          hoverBorderWidth:3,
          hoverBorderColor:'#000'
        }]
      },
      options:{
        title:{
          display:true,
          text:'BMI/ Country',
          fontSize:25
        },
        legend:{
          display:true,
          position:'right',
          labels:{
            fontColor:'#000'
          }
        },
        layout:{
          padding:{
            left:50,
            right:0,
            bottom:0,
            top:0
          }
        },
        tooltips:{
          enabled:true
        }
      }
    });
}
$(document).ready(function() {
	$('#preberiPredlogoBolnika').change(function() {
		$("#kreirajSporocilo").html("");
		var podatki = $(this).val().split(",");
		$("#kreirajIme").val(podatki[0]);
		$("#kreirajPriimek").val(podatki[1]);
  });
	
	$("#generiraj").click(function() {
		generirajPodatke(1);
		setTimeout(function() {
			generirajPodatke(2);
			setTimeout(function() {
				generirajPodatke(3);
			}, 150);
		}, 150);
	});
});